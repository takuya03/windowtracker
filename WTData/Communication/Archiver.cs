﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;
using WT.Data.Exceptions;

namespace WT.Data.Communication
{
    class Archiver
    {
        public Archiver()
        {
        }

        public Archiver(string filename) { Open(filename); }

        public Archiver(Stream stream) { Open(stream); }

        /// <summary>
        /// 現在開いているストリーム
        /// </summary>
        private Stream CurrentStream { get; set; }

        /// <summary>
        /// CurrentStreamが自分で開いたストリームであるか
        /// </summary>
        private bool IsThisOpened { get; set; }

        /// <summary>
        /// データベースファイル
        /// </summary>
        public string DBFile { get; private set; }

        /// <summary>
        /// 開いているか
        /// </summary>
        public bool IsOpen { get; private set; }

        /// <summary>
        /// Zipアーカイブ
        /// </summary>
        private ZipArchive Archive { get; set; }

        /// <summary>
        /// テーブル名一覧
        /// </summary>
        public string[] Tables
        {
            get
            {
                if (!IsOpen) return new string[0];
                var list = new List<string>();
                foreach (var item in Archive.Entries)
                {
                    list.Add(item.Name);
                }
                return list.ToArray();
            }
        }

        /// <summary>
        /// 開く
        /// </summary>
        private void Open()
        {
            IsOpen = true;
            if (CurrentStream.CanWrite)
            {
                Archive = new ZipArchive(CurrentStream, ZipArchiveMode.Update);
            }
            else
            {
                Archive = new ZipArchive(CurrentStream, ZipArchiveMode.Read);
            }
        }

        /// <summary>
        /// ストリームを指定して開く
        /// </summary>
        /// <param name="stream"></param>
        public void Open(Stream stream)
        {
            if (IsOpen) return;
            CurrentStream = stream;
            Open();
        }

        /// <summary>
        /// ファイル名を指定して開く
        /// </summary>
        /// <param name="path"></param>
        public void Open(string path)
        {
            FileMode mode;
            if (File.Exists(path))
            {
                mode = FileMode.Create;
            }
            else
            {
                mode = FileMode.Open;
            }
            DBFile = path;
            IsThisOpened = true;
            Open(new FileStream(path, mode));
        }

        /// <summary>
        /// データベースを閉じる
        /// </summary>
        public void Close()
        {
            if (!IsOpen) return;
            if (IsThisOpened)
            {
                CurrentStream?.Close();
            }
            CurrentStream = null;
            DBFile = null;
            IsThisOpened = false;
            IsOpen = false;
            Archive = null;
        }

        /// <summary>
        /// テーブルの作成
        /// </summary>
        /// <param name="tableName">作成するテーブル名</param>
        /// <exception cref="NotOpenedException">データベースが開かれていない</exception>
        /// <exception cref="FailedOperationException">指定されたテーブルが既に存在している</exception>
        public TableFile CreateTable(string tableName)
        {
            if (!IsOpen) throw new NotOpenedException();
            if (Archive.GetEntry(tableName) != null) throw new FailedOperationException("The specified table already exists.");
            return new TableFile(Archive.CreateEntry(tableName));
        }

        /// <summary>
        /// テーブルエントリーの取得
        /// </summary>
        /// <param name="tableName">テーブル名</param>
        /// <returns></returns>
        /// <exception cref="NotOpenedException">データベースが開かれていない</exception>
        public TableFile GetEntry(string tableName)
        {
            if (!IsOpen) throw new NotOpenedException();
            return new TableFile(Archive.GetEntry(tableName));
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    Close();
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~Controller() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
