﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace WT.Data.Communication
{
    class TableFile
    {
        public TableFile(ZipArchiveEntry entry)
        {
            Entry = entry;
        }
        
        /// <summary>
        /// 管理エントリー
        /// </summary>
        private ZipArchiveEntry Entry { get; }
        
    }
}
