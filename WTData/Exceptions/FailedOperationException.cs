﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WT.Data.Exceptions
{
    public class FailedOperationException : Exception
    {
        public FailedOperationException() : base() { }
        public FailedOperationException(string message) : base(message) { }
        public FailedOperationException(string message, Exception innerException) : base(message, innerException) { }
    }
}
