﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WT.Data.Exceptions
{
    public class NotOpenedException : Exception
    {
        public NotOpenedException() : base("File not opened.") { }
        public NotOpenedException(string message) : base(message) { }
        public NotOpenedException(string message, Exception innerException) : base(message, innerException) { }
    }
}
