﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WT.Tracker
{
    public class ProcessInfo
    {
        #region Extern user32.dll
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", EntryPoint = "GetWindowText", CharSet = CharSet.Auto)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);
        #endregion

        private static readonly int TextBufferSize = 1024;
        
        /// <summary>
        /// 現在の情報を取得
        /// </summary>
        /// <returns></returns>
        public static ProcessInfo CurrentInfo()
        {
            var t = DateTime.Now;
            var winText = new StringBuilder(TextBufferSize);
            var hWnd = GetForegroundWindow();
            GetWindowText(hWnd, winText, TextBufferSize);

            int id;
            GetWindowThreadProcessId(hWnd, out id);

            return new ProcessInfo()
            {
                TimeStamp = t,
                WindowText = winText.ToString(),
                Process = System.Diagnostics.Process.GetProcessById(id)
            };
        }

        /// <summary>
        /// タイムスタンプ
        /// </summary>
        public DateTime TimeStamp { get; private set; }

        /// <summary>
        /// ウィンドウ名
        /// </summary>
        public string WindowText { get; private set; }

        /// <summary>
        /// プロセス情報
        /// </summary>
        public System.Diagnostics.Process Process { get; private set; }
    }
}
