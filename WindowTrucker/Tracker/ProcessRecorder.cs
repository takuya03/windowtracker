﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Tracker
{
    public class ProcessRecorder
    {
        /// <summary>
        /// 
        /// </summary>
        public SortedDictionary<DateTime, RecordItem> RecordingList { get; } = new SortedDictionary<DateTime, RecordItem>();

        /// <summary>
        /// 記録
        /// </summary>
        public void Record()
        {
            var item = ProcessInfo.CurrentInfo();
            if(RecordingList.Count != 0)
            {
                var lastObj = RecordingList.Last();
                if((lastObj.Value.WindowText == item.WindowText) && (lastObj.Value.ProcessName == item.Process.ProcessName))
                {
                    lastObj.Value.EndTime = item.TimeStamp;
                    ChangedItem?.Invoke(this, EventArgs.Empty);
                    return;
                }
            }
            RecordingList.Add(item.TimeStamp, new RecordItem()
            {
                StartTime = item.TimeStamp,
                EndTime = item.TimeStamp,
                WindowText = item.WindowText,
                ProcessName = item.Process.ProcessName
            });
            ChangedItem?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler ChangedItem;
    }
}
