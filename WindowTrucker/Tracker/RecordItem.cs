﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WT.Tracker
{
    public class RecordItem
    {
        /// <summary>
        /// 開始時刻
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// 終了時刻
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Window名
        /// </summary>
        public string WindowText { get; set; }

        /// <summary>
        /// プロセス名
        /// </summary>
        public string ProcessName { get; set; }
    }
}
