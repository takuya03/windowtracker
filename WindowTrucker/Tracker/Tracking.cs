﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace WT.Tracker
{
    public class Tracking
    {
        public Tracking()
        {
            Timer.Tick += Timer_Tick;
        }

        /// <summary>
        /// タイマー
        /// </summary>
        private DispatcherTimer Timer { get; } = new DispatcherTimer(DispatcherPriority.Normal);

        /// <summary>
        /// 取得間隔
        /// </summary>
        public TimeSpan Interval { get; set; } = TimeSpan.FromSeconds(10);

        /// <summary>
        /// 動作中か
        /// </summary>
        public bool IsRun { get; private set; } = false;

        /// <summary>
        /// プロセスレコーダー
        /// </summary>
        public ProcessRecorder Recorder { get; set; } = new ProcessRecorder();

        /// <summary>
        /// トラッキングの開始
        /// </summary>
        public void Start()
        {
            if (IsRun) return;
            Timer.Interval = Interval;
            Timer.Start();
            IsRun = true;
            Started?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// トラッキングの停止
        /// </summary>
        public void Stop()
        {
            if (!IsRun) return;
            Timer.Stop();
            IsRun = false;
            Stoped?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// 定期実行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            Recorder?.Record();
        }
        
        /// <summary>
        /// 定期トラッキング開始イベント
        /// </summary>
        public event EventHandler Started;
        public event EventHandler Stoped;
    }
}
