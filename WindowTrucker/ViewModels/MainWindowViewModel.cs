﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Mvvm;

namespace WT.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        public MainWindowViewModel()
        {
            Tracker.Recorder.ChangedItem += Recorder_ChangedItem;
        }

        public Tracker.Tracking Tracker { get; } = new Tracker.Tracking();

        public IEnumerable<Tracker.RecordItem> RecordItems
        {
            get
            {
                return new List<Tracker.RecordItem>(Tracker.Recorder.RecordingList.Values);
            }
        }

        #region InitializeCommand
        private DelegateCommand _InitializeCommand;

        public DelegateCommand InitializeCommand
        {
            get
            {
                if (_InitializeCommand == null)
                {
                    _InitializeCommand = new DelegateCommand(Initialize);
                }
                return _InitializeCommand;
            }
        }

        private void Initialize()
        {
            // TODO: コマンドの内容
            Tracker.Start();
            Tracker.Recorder.Record();
        }
        #endregion


        private void Recorder_ChangedItem(object sender, EventArgs e)
        {
            RaisePropertyChanged(nameof(RecordItems));
        }
    }
}
